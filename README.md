This is a Slack<->IRC bridge.  Forked from https://github.com/ziozzang/slack-irc-bridge. Original code is forked from https://github.com/voldyman/slack-irc-bridge.

It is highly modified and specialized for our purposes at http://bitraf.no, a makerspace in Oslo.

No support will be given. Caution: This needs a rewrite but is fine for now :)
